<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>User Login From</title>

	<!-- Bootstrap 4 CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- Social icons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- my css -->
	<link rel="stylesheet" type="text/css" href="style.css">


<style>
		
.fa {
  padding: 10px;
  font-size: 15px;
  width: 40px;
  text-align: center;
  text-decoration: none;
  margin: 5px 15px 5px 11px;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

</style>

</head>

<body>

<div class="container">
	<div class="row">
		<div class="col-md-4 offset-md-4  from-div">
			<form action="" method="">
				<h3 class="text-center text-info ">User Login</h3>

				<div class="form-group">
					<label for="email">Username or Email :</label>
					<input type="email" name="" class="form-control form-control-lg">
				</div>
				<div class="form-group">
					<label for="password">Password :</label>
					<input type="password" name="" class="form-control form-control-lg" >
				</div>
				
				<div class="form-group">
					<button  type="submit" class="btn btn-primary btn-block btn-lg">Sing In</button>
				</div>
				<p class="text-center">Not yet a register ?<a href="#">Sing Up</a></p>


				<div class="text-center text-info">
						<h6>Login with</h6>
				</div>
				
				<a href="#" class="fa fa-facebook"></a>
				<a href="#" class="fa fa-twitter"></a>
				<a href="#" class="fa fa-google"></a>
				<a href="#" class="fa fa-linkedin"></a>
				<a href="#" class="fa fa-pinterest"></a>	
					
				
				
			</form>

		</div>	
	</div>
	
</div>


</body>
</html>
